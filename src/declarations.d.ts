declare module "*.scss";

interface RefObject<T> {
  // immutable
  readonly current: T | null;
}

export function createRef<T>(): RefObject<T>;
