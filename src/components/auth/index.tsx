import React, { Component } from "react";

import { observer, inject } from "mobx-react";
import styles from "./styles/index.scss";
import Logo from "../public/Logo/";
import Input from "../public/Input";
import Button from "../public/Button";

interface MyProps {
  [key: string]: any; // напсиать интерфейс для стора и взять тут его в props
}

@inject("authorize")
@observer
class Auth extends Component<MyProps> {
  public changeValueInput(elem: HTMLInputElement, callback: any) {
    console.log(elem.value, "elem");
    console.log(callback, "callback");
  }

  render() {
    return (
      <div className={styles.auth_container}>
        <div className={styles.container_form}>
          <Logo type="small" />
          <div className={styles.content_form}>
            <div className={styles.form_discription}>
              <h3>Sign in</h3>
              <span>
                Hello there ! sign in and start managing your trueDiamond
                account
              </span>
            </div>

            <div className={styles.form}>
              {inputFieldDefault.map(input => {
                const { name, callbackProps, style, id } = input;

                return (
                  <div className={styles.input_container} key={id}>
                    <Input
                      changeFunction={this.changeValueInput.bind(this)}
                      callbackProps={callbackProps}
                      style={style}
                    />
                    <span className={styles.placeholder_input}>{name}</span>
                  </div>
                );
              })}
            </div>

            <Button
              name="sign In"
              onPressFunction={() => this.props.authorize.setStatusAuth(true)}
              style={styles.button_signIn}
            />
          </div>
        </div>
      </div>
    );
  }
}

const inputFieldDefault = [
  {
    name: "login",
    callbackProps: "login",
    id: "unick_id_login",
    style: styles.input
  },
  {
    name: "password",
    callbackProps: "password",
    id: "password_id_login",
    style: styles.input
  }
];

export default Auth;
