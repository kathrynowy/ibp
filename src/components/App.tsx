import * as React from "react";
import { observer, inject } from "mobx-react";
import { Switch, Route, Redirect } from "react-router-dom";
import Auth from "./auth";

import styles from "./styles/index.scss";

import "../styles/index.scss";
import MainPage from "./mainPage";

@inject("authorize") // внедряем только нужную ччасть стора в компонент
@observer // указали что компонент подписался на обновление этог остора
class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  public render(): JSX.Element {
    const { authorize } = this.props;
    console.log(authorize, "authorize - app");
    return (
      <div className={styles.App}>
        <Switch>
          <Route
            exact
            path={!authorize.isAuth ? "/authorize" : "/"}
            component={!authorize.isAuth ? Auth : MainPage}
          />
          >
          <Redirect to={!authorize.isAuth ? "/authorize" : "/"} />
        </Switch>
      </div>
    );
  }

  componentDidMount() {}
}

export default App;
