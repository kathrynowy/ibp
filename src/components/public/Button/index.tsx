import React from "react";

interface MyProps {
  name: string;
  onPressFunction(): void;
  style?: string;
}

const Button: React.FunctionComponent<MyProps> = ({
  name,
  onPressFunction,
  style
}) => {
  console.log(style);
  return (
    <button className={style && style} onClick={onPressFunction}>
      {name}
    </button>
  );
};
export default Button;
