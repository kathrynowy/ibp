import React from "react";

interface MyProps {
  type: string;
}

const Logo: React.FunctionComponent<MyProps> = ({ type }) => {
  switch (type) {
    case "small":
      return <span>small logo</span>;
    case "medium":
      return <span>medium logo</span>;
    case "big":
      return <span>big logo</span>;

    default:
      return <span>error name logo</span>;
  }
};

export default Logo;
