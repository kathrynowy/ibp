import React, { Component } from "react";

interface MyProps {
  changeFunction(elem: HTMLInputElement | null, arg?: any): void;
  callbackProps: any;
  style?: string;
}

interface MyState {}

class Input extends Component<MyProps, MyState> {
  private input = React.createRef<HTMLInputElement>();

  render() {
    const { changeFunction, callbackProps, style } = this.props;

    return (
      <input
        className={style && style}
        ref={this.input}
        placeholder="default"
        onChange={() => changeFunction(this.input.current, callbackProps)}
      />
    );
  }
}

export default Input;
