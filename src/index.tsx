import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { observable, configure } from "mobx";
import { Provider } from "mobx-react";

import { BrowserRouter } from "react-router-dom";

import mainStore from "./classes/mainStore";
import authorize from "./classes/authorize";

configure({ enforceActions: "observed" });

const appStore = observable({
  mainStore,
  authorize
});

ReactDOM.render(
  <BrowserRouter>
    <Provider {...appStore}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);
