class MainStore {
  nameUser: string;
  constructor() {
    /**
     * Инициализация дочерних хранилищ
     */
    this.nameUser = "";
  }
}

const mainStore = new MainStore();

export default mainStore;
