import { observable, computed, autorun, reaction, get, action } from "mobx";

class Authorize {
  @observable isAuth: boolean;

  constructor() {
    this.isAuth = false;
  }

  @action("setStateisAuth")
  setStatusAuth(status: boolean) {
    console.log("status,status", status);
    this.isAuth = status;
  }
}

const authorize = new Authorize();

export default authorize;
