"use strict";

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env, argv) => {
  const isDevelopment = argv.mode !== "production";

  return {
    devServer: {
      host: "localhost",
      port: "3000",
      hot: true,
      headers: {
        "Access-Control-Allow-Origin": "*"
      },
      historyApiFallback: true
    },
    // webpack will take the files from ./src/index
    entry: "./src/index",

    // and output it into /dist as bundle.js
    output: {
      path: path.join(__dirname, "/dist"),
      filename: "bundle.js"
    },

    // adding .ts and .tsx to resolve.extensions will help babel look for .ts and .tsx files to transpile
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".scss"]
    },

    module: {
      rules: [babelLoader, moduleScss]
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: "./src/index.html"
      })
    ]
  };
};

// we use babel-loader to load our jsx and tsx files
const babelLoader = {
  test: /\.(ts|js)x?$/,
  exclude: /node_modules/,
  use: {
    loader: "babel-loader"
  }
};

const moduleScss = {
  test: /\.scss$/,
  use: [
    { loader: "style-loader" },
    { loader: "css-modules-typescript-loader" },
    { loader: "css-loader", options: { modules: true } },
    { loader: "sass-loader" } // to convert SASS to CSS
  ]
};
